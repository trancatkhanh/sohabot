const knex = require('knex');

const Knex = knex({
    client: process.env.KNEX_CLIENT || 'mysql',
    connection: {
        host : process.env.KNEX_CLIENT_HOST || 'localhost',
        user : process.env.KNEX_CLIENT_USER || 'root',
        password : process.env.KNEX_CLIENT_PASSWORD || 'Diablo321',
        database : process.env.KNEX_CLIENT_DATABASE || 'node'
    }
});

module.exports = Knex