const Joi = require('joi');
const createUser = Joi.object({
    name: Joi.string()
        .min(3)
        .max(30)
        .required()
})

const validate = {
    createUser: (payload) => createUser.validate(payload)
}

module.exports = validate