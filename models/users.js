const Knex = require('../config/knex')
const Validate = require('../utils/validate')

module.exports = {
    fetchAll: () => {
        return Knex.from('user').select()
    },

    createUser: async(payload) => {
        const { name } = payload
        const validated = Validate.createUser(payload)
        if (validated.error) {
            const message = validated.error.details[0].message
            return { statusCode: 403, data: { error: message } }
        }
        const createdUser = await Knex.from('user').insert({name}).returning('*')
        return {
            statusCode: 200,
            data: { createdUser }
        }
    }
}