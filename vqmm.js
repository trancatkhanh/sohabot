import event_helpers from "/project/all/both/js/event_helpers.js";
var event = null;
$(document).ready(() => {
    $("canvas").attr("width", $(".wheel").width()), $("canvas").attr("height", $(".wheel").height()), event = new vqmm, $(".btn-submit").on("click", function(e) {
        e.preventDefault(), event.submitGameInfo()
    }), $(".btn-dice").on("click", function() {
        event.startSpin(1)
    }), $(".btn_10").on("click", function() {
        event.startSpin(10)
    }), $(".btn_50").on("click", function() {
        event.startSpin(50)
    }), $("#hisBtn").on("click", () => {
        event.history()
    }), $(".btn_getBonus").on("click", () => {
        event.bonus()
    }), $(".btn_themluot").on("click", () => {
        event.event_helpers.redirect(event.url.payment, !0)
    }), $(".doicode").on("click", function() {
        let type = $(this).attr("attr-type");
        event.getCode(type)
    }), $(".btn_login").on("click", function() {
        event.checkin()
    })
});
class vqmm {
    constructor() {
        this.player = null, this.flag = 0, this.wheelSpinning = !1, this.event_helpers = new event_helpers, this.theWheel = null, this.url = {}, this.lucky_players = null, this.config(), this.getInfo(), this.initWheel(), this.loop()
    }
    loop() {
        setInterval(() => {
            this.update()
        }, 100)
    }
    update() {
        this.updatePlayer(), this.updateLuckyPlayers()
    }
    updatePlayer() {
        let player = this.player;
        player && ($("#turn_left").html(player.turn_left), $("#score").html(player.score), $("#all_played").html(player.global_turn), $("#char_name").html(player.opt2))
    }
    updateLuckyPlayers() {
        let lucky_players = this.lucky_players;
        if (lucky_players) {
            let html = "";
            $.each(lucky_players, (key, value) => {
                html += `<p>${value.character_name} đã nhận ${value.data_receive}</p>`
            }), $("#translate").html(html)
        }
    }
    config() {
        this.url.get_info = "/vqmm/getInfo", this.url.spin = "/vqmm/makeSpin", this.url.leader_board = "/vqmm/getLeaderBoard", this.url.char_list = "/vqmm/getCharList", this.url.submit_info = "/vqmm/saveGameInfo", this.url.history = "/vqmm/history", this.url.top = "/vqmm/top", this.url.bonus = "/vqmm/bonus", this.url.checkin = "/vqmm/checkin", this.url.getCode = "/vqmm/getCode", this.url.payment = "https://nap.sohagame.vn/vethan"
    }
    getInfo() {
        this.event_helpers.sendXhr(this.url.get_info, "POST", {}).then(data => {
            if (data.message && this.event_helpers.fireMessageBox({
                html: `<span class="text-warning">${data.message}</span>`
            }), 0 == data.check_login) {
                let loginUrl = this.event_helpers.getLoginSohagameUrl();
                this.event_helpers.fireMessageBox({
                    html: `Cần đăng nhập để tham gia sự kiện <hr> <a class="btn btn-warning" href="${loginUrl}">Đăng nhập</a>`,
                    showConfirmButton: !1
                })
            }
            if (0 == data.registed) return this.getCharlist(), !1;
            if (data.player && (this.player = data.player, this.player.json_params)) {
                let counter = JSON.parse(this.player.json_params);
                console.log(counter), $.each(counter, (key, value) => {
                    $(`.${key}`).html(`(${value})`)
                })
            }
            data.lucky_players && (this.lucky_players = data.lucky_players), this.top()
        })
    }
    top() {
        this.event_helpers.sendXhr(this.url.top, "POST", {}).then(data => {
            if (data.top_score && data.top_played) {
                let html = "";
                $.each(data.top_score, (key, top) => {
                    html += `<tr class="th"><td>${key+1}</td><td>${top.opt2}</td><td>${top.score}</td></tr>`
                }), $("#top_score").html(html);
                let html1 = "";
                $.each(data.top_played, (key, top) => {
                    html1 += `<tr class="th"><td>${key+1}</td><td>${top.opt2}</td><td>${top.turn_left}</td></tr>`
                }), $("#top_play_number").html(html1)
            }
        })
    }
    getCharlist() {
        this.event_helpers.sendXhr(this.url.char_list, "POST", {}).then(data => {
            if (data) {
                $("#popup-register").modal("show");
                let sever_list = [];
                $.each(data, (key, sever) => {
                    "" != sever.char_list[0].roleid && sever_list.push(sever)
                }), $.each(sever_list, (key, sever) => {
                    $("#sServer").append(`<option value="${sever.areaname}" attr-char='${JSON.stringify(sever.char_list)}'>${sever.areaname}</option>`)
                }), $("#sServer").on("change", () => {
                    this.renderChar()
                })
            }
        })
    }
    renderChar() {
        let selected_sever = $("#sServer option:selected")[0],
            char_list = $(selected_sever).attr("attr-char");
        void 0 !== char_list && ($("#sChar").empty(), $.each(JSON.parse(char_list), (key, char) => {
            $("#sChar").append(`<option value="${char.rolename}">${char.rolename}</option>`)
        }))
    }
    submitGameInfo() {
        let sever_name = $("#sServer").val(),
            char_name = $("#sChar").val();
        "" != sever_name && "" != char_name && this.event_helpers.sendXhr(this.url.submit_info, "POST", {
            sever_name: sever_name,
            char_name: char_name
        }).then(data => {
            data.player && (this.player = data.player), Swal.fire("Nhập thông tin thành công"), $(".close").trigger("click"), $("#popup-register").modal("hide")
        })
    }
    checkin() {
        this.event_helpers.sendXhr(this.url.checkin, "POST", {}).then(data => {
            this.getInfo(), data.message && this.event_helpers.fireMessageBox(data.message)
        })
    }
    initWheel() {
        $("#canvas"), this.event_helpers.is_mobile;
        let scaleWidth = $(".dice_prize").width();
        $("#canvas").attr("width", scaleWidth), $("#canvas").attr("height", scaleWidth);
        let theWheel = new Winwheel({
                numSegments: 11,
                outerRadius: 150,
                drawMode: "image",
                responsive: !0,
                drawText: !1,
                segments: [{
                    text: "T-55 Vampire"
                }, {
                    text: "P-40 Kittyhawk"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }, {
                    text: "North American Harvard"
                }],
                animation: {
                    type: "spinToStop",
                    duration: 5,
                    spins: 8
                }
            }),
            loadedImg = new Image,
            img_source = null;
        this.event_helpers.sendXhr("/vqmm/getImageSource", "POST", {
            w: $(".wheel").width(),
            h: $(".wheel").height()
        }).then(data => {
            img_source = data, loadedImg.src = img_source, $("#source_image").attr("src", data), loadedImg.onload = function() {
                theWheel.wheelImage = loadedImg, theWheel.draw()
            }
        }), this.theWheel = theWheel
    }
    startSpin(time) {
        0 == this.wheelSpinning && (this.theWheel.animation.spins = 8, this.event_helpers.sendXhr(this.url.spin, "POST", {
            spin: time
        }).then(data => {
            data.message && this.event_helpers.fireMessageBox(data.message);
            let html = "";
            data.lucky_players && (this.lucky_players = data.lucky_players), data.most_value_prize && (this.theWheel.stopAnimation(!1), this.theWheel.rotationAngle = 0, this.theWheel.draw(), this.theWheel.animation.stopAngle = data.most_value_prize.stopAt, this.wheelSpinning = !0, this.theWheel.startAnimation(), data.total && (html += `<h3 class="text-warning">Bạn nhận được : ${data.total} điểm</h3>`), data.prizes && $.each(data.prizes, (key, prize) => {
                let badge = "badge-success",
                    style = "";
                prize.score == data.most_value_prize.score && (badge = "badge-warning"), 0 == prize.score && (badge = "badge-dark"), "Hoàng Kim Bảo Hạp" == prize.text && (badge = "badge-danger"), prize.limit && (badge = "badge-danger", style = "background: #ff5e00;"), html += `<span class="text-light badge ${badge} margin-10" style="margin:5px; font-size:1em; ${style}"> ${prize.text} </span>`
            })), "" != html && setTimeout(() => {
                this.wheelSpinning = !1, this.event_helpers.fireMessageBox({
                    html: html
                }), data.player && (this.player = data.player)
            }, 5e3), this.getInfo()
        }))
    }
    history() {
        this.event_helpers.sendXhr(this.url.history, "POST", {}).then(data => {
            $(".table_history").find("tbody").empty(), $(".tab_content_history ").attr("style", "overflow-y:scroll; height:150px;");
            let html = "";
            $.each(data, (key, log) => {
                html += `<tr class="th"><td>${key}</td><td>${log.text}</td><td>${log.time}</td></tr>`
            }), $(".table_history").find("tbody").html(html)
        })
    }
    bonus() {
        this.event_helpers.sendXhr(this.url.bonus, "POST", {}).then(data => {
            data.player && (this.player = data.player), data.giftcode && this.event_helpers.fireMessageBox(`Giftcode của bạn là: ${data.giftcode}`), data.message && this.event_helpers.fireMessageBox(data.message)
        })
    }
    getCode(type) {
        this.event_helpers.sendXhr(this.url.getCode, "POST", {
            type: type
        }).then(data => {
            data.player && (this.player = data.player, this.getInfo()), data.message && this.event_helpers.fireMessageBox(`${data.message}`)
        })
    }
}