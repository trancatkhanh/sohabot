export default class event_helpers {
    constructor() {
        if (typeof Swal == 'undefined') {
            $(`body`).prepend(`<script src="//${window.location.host}/project/all/both/js/sweetalert2.min.js"></script>`);
        }
    }

    async sendXhr(url, type, data) {
        data.pub_token = pub_token;
        return await $.ajax({
            url: url,
            type: type,
            data: data,
        });
    }

    swalInit() {

    }

    fbInit(app_id) {//2036502456566270
        if (typeof FB == 'undefined') {
            $(`body`).append(`<script>
                              window.fbAsyncInit = function() {
                                FB.init({
                                  appId            : ${app_id},
                                  autoLogAppEvents : true,
                                  xfbml            : true,
                                  version          : 'v7.0'
                                });
                              };
                            </script>
            <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>`);
        }
    }

    loginSohagame() {
        window.location = this.getLoginSohagameUrl();
    }

    logoutSohagame() {
        window.location = this.getLogoutSohagameUrl();
    }

    getLoginSohagameUrl() {
        return window.location.origin + `/oauth/request/index.php?callback=${window.location.href}`;
    }

    getLogoutSohagameUrl() {
        return window.location.origin + `/auth/logout?callback=${window.location.href}`;
    }

    copyToClipboard(element, callback = null) {
        var $temp = $("<input id='tempcopy'>");
        $("body").append($temp);
        $temp.val($(element).val()).select();
        document.execCommand("copy");
        Swal.fire('ÄĂ£ copy');
        if (callback != null) {
            callback;
        }
        $temp.remove();
    };

    fireMessageBox(data) {
        if (typeof data != 'object') {
            Swal.fire(data);
            return false;
        }
        Swal.fire(
            {
                title: this.get_default(data.title, ""),
                html: this.get_default(data.html, ""),
                showConfirmButton: this.get_default(data.showConfirmButton, true),
                scrollbarPadding: true,
                width: 480
            }
        );
    }

    get_default(variable, default_param = "") {
        return typeof variable != 'undefined' ? variable : default_param;
    }

    redirect(url, newtab = null) {
        Swal.fire({
            title: '<strong>Chuyá»ƒn hÆ°á»›ng</strong>',
            icon: 'info',
            html: `TrĂ¬nh duyá»‡t sáº½ chuyá»ƒn hÆ°á»›ng sang URL : <a href="javascript:void(0);">${url}</a>`,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText:
                '<i class="fa fa-thumbs-up"></i> Äá»“ng Ă½!',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText:
                '<i class="fa fa-thumbs-down"></i> Há»§y',
            cancelButtonAriaLabel: 'Thumbs down'
        }).then(result => {
            if (result.value) {
                if (newtab != null) {
                    window.open(url, '_blank');
                    return false;
                }
                window.location = url;
                return false;
            } else {
                return false;
            }
        });

    }

    showPopup(show, severList, limit=null) {
        let popup_server = $('.popup_server');
        if (show == true) {
            console.log(true);
            let sever_list = [];
            $(`#choose_server`).empty();
            $(`#choose_character`).empty();
            $.each(severList, (key, sever) => {
                if (sever.char_list[0].roleid != "") {
                    sever_list.push(sever);
                }
            });
            $.each(sever_list, (key, sever) => {
                $(`#choose_server`).append(`<option value="${sever.areaname}" attr-char='${JSON.stringify(sever.char_list)}'>${sever.areaname}</option>`);
            });
            let selected_sever = $(`#choose_server option:selected`)[0];
            let char_list = $(selected_sever).attr('attr-char');
            if (typeof char_list != 'undefined') {
                $(`#choose_character`).empty();
                $.each(JSON.parse(char_list), (key, char) => {
                    if(limit!=null && parseInt(char.level) >= limit){
                        $(`#choose_character`).append(`<option value="${char.rolename}">${char.rolename}</option>`);
                    }else{
                        $(`#choose_character`).append(`<option value="${char.rolename}">${char.rolename}</option>`);
                    }

                });
            }
            popup_server.show();
            $(`.close_popup`).off('click');
            $(`.close_popup`).click(() => {
                popup_server.hide();
            });
        } else {
            popup_server.hide();
        }
    }

    renderCharList(limit=null) {
        let selected_sever = $(`#choose_server option:selected`)[0];
        let char_list = $(selected_sever).attr('attr-char');
        if (typeof char_list != 'undefined') {
            $(`#choose_character`).empty();
            $.each(JSON.parse(char_list), (key, char) => {
                // console.log(char.level);
                if(limit != null){
                    if(parseInt(char.level) >= limit){

                        $(`#choose_character`).append(`<option value="${char.rolename}">${char.rolename} - LV.${char.level}</option>`);
                    }
                }else{
                    $(`#choose_character`).append(`<option value="${char.rolename}">${char.rolename}</option>`);
                }

            });
        }
    }

    logTable(items,fields){
        let html = ``;
        $.each(items,function(key,item){
            html += `<tr>`;
            $.each(fields,(index,field)=>{

                html += `<td>${item[field]}</td>`;
            });
            html += `</tr>`;
        });
        html += ``;
        return html;
    }

    is_mobile() {
        let isMobile = false; //initiate as false
// device detection
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
            isMobile = true;
        }

        return isMobile;
    }
}


// export default event_helpers;