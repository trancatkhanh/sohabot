const jsonServer = require('json-server')
const server = jsonServer.create()
const { config } = require('./config/config')
const { NODE_ENV, SERVER_PORT } = config
const axios = require('axios');
import qs from 'qs';
const phantom = require('phantom');
const puppeteer = require('puppeteer');

server.use(jsonServer.bodyParser)

const Users = require('./models/users')

server.get('/version', async (req, res) => {
  res.status(200).send("Hello world version 1.0")
})

server.get('/user/:id', async (req, res) => {
  const user = await Users.fetchAll()
  res.status(200).send(user)
})

server.post('/bot', async (req, res) => {
  const { name, from, to } = req.body;
  const length = to + 1
  const url = 'https://soap.soha.vn/api/a/GET/auth/registerSh'
  const password = '123456'

  for (let i = from; i < length; i++) {
    // const number = 1 + i
    setTimeout(async() => {
      const username = name + i
      const email = `${username}@gmail.com`

      const postData = {
        app_id: 'b26113ae1121f96b479a74b7d32b6197',
        username: username,
        email: email,
        phone: email,
        password: password,
        pword: password,
        lang_code: 'vi',
      }
      // console.log(postData)
      const options = {
        method: 'POST',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        data: qs.stringify(postData),
        url,
      };
      const result = await axios(options);
      if(i === 200) {
        console.log(postData)
      }
      console.log(result.data)
    }, 2000)
  }

  res.status(200).send({msg:'ok'})
})

server.post('/botLogin', async (req, res) => {
  const { email } = req.body;
  const url = 'https://soap.soha.vn/api/a/get/auth/loginShg'
  const password = '123456'

  const postData = {
    app_id: 'b26113ae1121f96b479a74b7d32b6197',
    email: email,
    password: password,
    access_token: 'c29hcHRva2VuMC40MDQ2MjgwMCAxNjAxNTMyOTg0KzY0NjQ3Mzk3Mg==',
    lang_code: 'vi',
  }

  const options = {
    method: 'POST',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    data: qs.stringify(postData),
    url,
  };
  const result = await axios(options);
  console.log(result.data)
  if(result.data) {
    const red = await axios({
      method: 'GET',
      // headers: { 'content-type': 'application/x-www-form-urlencoded' },
      // data: qs.stringify(postData),
      url: 'https://tamquoctocchien.vn',
    })
    console.log(red)
  }

  res.status(200).send({msg:'ok'})
})

server.post('/user', async (req, res) => {
  const { name } = req.body;
  const result = await Users.createUser({name})
  const { statusCode, data } = result
  res.status(statusCode).send({data})
})

server.get('/crawler', async (req, res) => {

  const screenshot = 'tamquoc.png'
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto('https://tamquoctocchien.vn/oauth/request/index.php?callback=https://tamquoctocchien.vn/trung-thu')
  console.log("step 1: đi đến trang đăng nhập")

  await page.waitForSelector('input[type="email"]')
  await page.type('input[type="email"]', 'kingasawa@gmail.com')
  console.log("step 2: nhập email")

  await page.click('button.next-step')
  console.log("step 3: bấm vào button next")

  await page.waitForSelector('input[type="password"]')
  await page.type('input[type="password"]', 'Khanhtran123')
  console.log("step 4: Nhập mật khẩu")

  await page.click('button.btn_login')
  console.log("step 5: Bấm vào login để đăng nhập")

  // await page.goto('https://tamquoctocchien.vn/trung-thu')
  // console.log("step 6: Đi đến trang sự kiện trung thu")
  page.evaluate(_ => {
    window.scrollBy(0, 400);
  });

  await page.waitForSelector('a[title="Dạ tiệc trung thu"]')
  await page.click('a[title="Dạ tiệc trung thu"]')
  console.log("step 6: Bấm vào Nhận lượt quay")
  await page.screenshot({path: 'step_6.png'})

  // await page.waitForSelector('ul.tab-select')
  // const elHandleArray = await page.$$('ul.tab-select')
  // console.log(elHandleArray)
  await page.waitForSelector('a.btn_login')
  await page.evaluate(()=>document.querySelector('a.btn_login').click())
  console.log("step 7: Nhận được 3 lượt quay may mắn")
  await page.screenshot({path: 'step_7.png'})

  await page.waitForSelector('ul.tab_select')
  await page.evaluate(()=>document.querySelector('ul.tab_select li:nth-child(0n+2)').click())
  console.log("step 8: Bấm vào vòng quay")
  await page.screenshot({path: 'step_8.png'})

  await page.evaluate(()=>document.querySelector('#spin_button').click())
  console.log("step 9: Bắt đầu quay lần 1")
  await page.screenshot({path: 'step_9.png'})

  await page.waitForSelector('div.swal2-actions > button.swal2-confirm')
  await page.evaluate(()=>document.querySelector('div.swal2-actions > button.swal2-confirm').click())
  console.log("step 10 Nhận quà lần 1")
  await page.screenshot({path: 'step_10.png'})

  await page.evaluate(()=>document.querySelector('#spin_button').click())
  console.log("step 11: Bắt đầu quay lần 2")
  await page.screenshot({path: 'step_11.png'})

  await page.waitForSelector('div.swal2-actions > button.swal2-confirm')
  await page.evaluate(()=>document.querySelector('div.swal2-actions > button.swal2-confirm').click())
  console.log("step 12: Nhận quà lần 2")
  await page.screenshot({path: 'step_12.png'})

  await page.evaluate(()=>document.querySelector('#spin_button').click())
  console.log("step 13: Bắt đầu quay lần 3")
  await page.screenshot({path: 'step_13.png'})

  await page.waitForSelector('div.swal2-actions > button.swal2-confirm')
  await page.evaluate(()=>document.querySelector('div.swal2-actions > button.swal2-confirm').click())
  console.log("step 14: Nhận quà lần 3")
  await page.screenshot({path: 'step_14.png'})
  // swal2-confirm swal2-styled
  // li:nth-child(0n+1)
  // spin_button
  await page.screenshot({path: 'step_cuoi.png'})
  console.log('close browser')
  await browser.close()
  res.status(200).send({msg:'ok'})
})

server.listen(SERVER_PORT, () => {
  console.log('Server is running on PORT', SERVER_PORT, NODE_ENV)
})
